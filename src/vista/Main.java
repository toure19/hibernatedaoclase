/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Profesor;
import org.hibernate.Session;
import persistencia.hibernate.util.HibernateUtil;

/**
 *
 * @author usuario
 */
public class Main {

    public static void main(String[] args) {
        HibernateUtil.buildSessionFactory();

        try {
            HibernateUtil.openSessionAndBindToThread();

            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Profesor profesor = (Profesor) session.get(Profesor.class, 1001);
            System.out.println(profesor.toString());
        } finally {
            HibernateUtil.closeSessionAndUnbindFromThread();
        }

        HibernateUtil.closeSessionFactory();
    }
}
